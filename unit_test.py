import unittest

import os
import shutil
import lib.Database
import lib.Table


# Creere DB
try:
    shutil.rmtree("./data")
except:
    pass

db = None
class TestTable(unittest.TestCase):

    def test_01_db_create(self):
        crashed = False
        try:
            lib.Database.Database.create_database("./data/myDB")
        except:
            crashed = True
        self.assertNotEqual(crashed, True)
    
    def test_02_table_create(self):
        global db
        fields = [
            {"name": "nr_matricol", "type": str},
            {"name": "varsta", "type": float},
            {"name": "nume", "type": str},
            {"name": "prenume", "type": str}
        ]
        crashed = False
        try:
            lib.Table.Table.create_table("./data/myDB/studenti", fields)
            db = lib.Table.Table("./data/myDB/studenti")
        except:
            crashed = True
        self.assertNotEqual(crashed, True)
        self.assertIsNotNone(db)
    
    def test_03_insert(self):
        global db
        db = lib.Table.Table(os.path.join(os.getcwd(), "data", "myDB", "studenti"))
        db.insert({"nr_matricol": "1001SL1023", "varsta": 23, "nume": "Ionescu", "prenume": "Gigel"})
        db.insert({"nr_matricol": "1001SL2034", "varsta": 22, "nume": "Popescu", "prenume": "Dorel"})
        db.insert({"nr_matricol": "1001SL2035", "varsta": 21, "nume": "Georgescu", "prenume": "Ionel"})
        db.insert({"nr_matricol": "1001SL2093", "varsta": 38, "nume": "Ackerman", "prenume": "Levi"})
        result = db.query({})
        self.assertEqual(len(result.get('rows', [])), 4)
    
    def test_04_query_eq(self):
        global db
        q = {"varsta" : {"eq" : 38}}
        result = db.query(q)
        self.assertEqual(result.get('rows',[])[0], '1001SL2093|38.0|Ackerman|Levi')
    
    def test_05_query_lte(self):
        global db
        q = {"varsta" : {"lte" : 22}}
        result = db.query(q)
        self.assertEqual(result.get('rows',[])[0], '1001SL2034|22.0|Popescu|Dorel')
        self.assertEqual(result.get('rows',[])[1], '1001SL2035|21.0|Georgescu|Ionel')
    
    def test_06_query_el(self):
        global db
        q = {"prenume" : {"contains" : "el"}}
        result = db.query(q)
        self.assertEqual(result.get('rows',[])[0], '1001SL1023|23.0|Ionescu|Gigel')
        self.assertEqual(result.get('rows',[])[1], '1001SL2034|22.0|Popescu|Dorel')
        self.assertEqual(result.get('rows',[])[2], '1001SL2035|21.0|Georgescu|Ionel')
    
    def test_07_update(self):
        global db
        db.update({"prenume": {"eq": "Levi"}}, {"varsta": {"add": 1}})
        result = db.query({"prenume": {"eq": "Levi"}})
        self.assertEqual(result.get('rows', [])[0], '1001SL2093|39.0|Ackerman|Levi')
        
    def test_08_delete(self):
        global db
        db.delete({"prenume" : {"contains" : "ev"}})
        result = db.query({"prenume": {"eq": "Levi"}})
        self.assertEqual(len(result.get('rows', [])) , 0)

    def test_09_export_csv(self):
        global db
        try:
            os.unlink('temp.csv')
        except:
            pass

        db.export_csv('temp.csv', overwrite=True)


        with open('temp.csv', 'r') as f:
            data = f.read()
            self.assertNotEqual(len(data), 0)
        os.unlink('temp.csv')

    
    def test_10_export_text(self):
        global db
        try:
            os.unlink('temp.txt')
        except:
            pass

        db.export_text('temp.txt', overwrite=True)
        
        with open('temp.txt', 'r') as f:
            data = f.read()
            self.assertNotEqual(len(data), 0) 
        os.unlink('temp.txt')

    def test_11_parse_qstring(self):
        global db
        qstring = 'nr_matricol < 5 and b >= 10 and nume contains test'
        # b trebuie ignorat
        qdoc = db.parse_query(qstring)
        self.assertEqual(qdoc, {"nr_matricol" : {"lt" : '5'}, "nume" : {"contains" : "test"}})
    
    def test_12_parse_ustring(self):
        global db
        qstring = 'varsta += 5 and nr_matricol = dsa and test -= 1'
        qdoc = db.parse_update(qstring)
        self.assertEqual(qdoc, {"varsta" : {"add" : 5}, "nr_matricol" : {"set" : "dsa"}})

    def test_13_import_after_export(self):
        global db
        try:
            os.unlink('temp.csv')
        except:
            pass
        db.export_csv('temp.csv', overwrite=True)

        db_new_path = db.import_table_from_csv('temp.csv', table_name='testImport',database_dir='./data/myDB')
        db2 = lib.Table.Table("./data/myDB/studenti")
        result1 = db.query({})
        result2 = db2.query({})
        self.assertEqual(result1, result2)

    def test_14_create_db_invalid_path(self):
        lib.Database.Database.create_database("./$*")
    
    def test_15_create_table_invalid_fields(self):
        global db
        fields = [
            {"name": 2, "type": "HAHA"}
        ]
        
        with self.assertRaises(lib.Table.TableException) as cm:
            lib.Table.Table.create_table("./data/myDB/test_bad_fields", fields)
        the_exception = cm.exception

        self.assertEqual(the_exception.message, "Invalid type")

    def test_16_insert_bad_fields(self):
        global db
        with self.assertRaises(lib.Table.TableException) as cm:
            db.insert({"nr_matricol": "1001SL1023", "varsta": 23, "nume": "Ionescu", "prenume_FALS": "Gigel"})
        the_exception = cm.exception
        self.assertEqual(the_exception.message, "Inserted row not matching schema.")

    def test_17_doc_matches_query(self):
        global db
        self.assertEqual(db.doc_matches_query({"test" : 3}, {"test" : {"eq" : 3}}), True)
        self.assertEqual(db.doc_matches_query({"test" : 3}, {"test" : {"lte" : 3}}), True)
        self.assertEqual(db.doc_matches_query({"test" : "abcd"}, {"test" : {"contains" : "bc"}}), True)

    def test_18_delete_unexisting(self):
        global db
        result = db.query({})
        db.delete({"nr_matricol" : {"eq" : "dxczxcxczxczx"}})
        result2 = db.query({})
        self.assertEqual(result, result2)

    def test_19_delete_at_indexes_big(self):
        global db
        result = db.query({})
        db.delete_at_indexes([55,33])
        result2 = db.query({})
        self.assertEqual(result, result2)

    def test_20_delete_at_index_0(self):
        global db
        result = db.query({})
        db.delete_at_indexes([0])
        result2 = db.query({})
        self.assertEqual(result.get('rows')[1:], result2.get('rows'))

# db.export_csv('temp.csv', overwrite=True)
# lib.Table.Table.import_table('temp.csv')



if __name__ == '__main__':   
    unittest.main()