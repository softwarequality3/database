# O baza de date o sa fie doar un folder in care o sa avem tabele.
import os
import shutil
import unittest
from unittest.mock import MagicMock
try:
    import Table
    import Parser
except ImportError:
    import lib.Table as Table
    import lib.Parser as Parser
# from . import Table
# from . import Parser
import os


class DatabaseException(Exception):
    def __init__(self, message):
        super().__init__(message)
        self.errors = ""
        self.message = message


class Database:
    def __init__(self, path):
        assert os.path.isdir(path), "Invalid directory"
        self._path = path

    def get_tables(self):
        tables = {}
        for table in os.listdir(self._path):
            table_path = os.path.join(self._path, table)
            if not os.path.isdir(table_path):
                continue
            assert table not in tables, 'Broken invariant'
            tables[table] = table_path
        return tables

    def get_table(self, name):
        assert isinstance(name, str)
        tables = self.get_tables()
        if name not in tables:
            raise DatabaseException("Table {} not found in db {}".format(
                name, self._path))
        return Table.Table(tables[name])

    def select(self, query):
        assert hasattr(query, 'startswith'), "Query must bbe string"
        projected_columns, table_name, condition = Parser.parse_select(query)
        table = self.get_table(table_name)
        table_columns = table.columns()
        if projected_columns == ["*"]:
            projected_columns = table_columns
        columns = {}
        for c in projected_columns:
            try:
                i = table_columns.index(c)
            except ValueError:
                raise DatabaseException(
                    "Column {} does not exist in table {} of database {}".format(
                        c, table_name, self._path))
            assert c not in columns, 'Broken invariant. Conflicting columns.'
            columns[c] = i
        to_keep_i = set(columns.values())
        header = [c for c, i in sorted(columns.items(), key=lambda a: a[-1])]

        parsed_query_condition = table.parse_query(condition)
        result = table.query(parsed_query_condition)
        # print(header)
        indexes = result["indexes"]
        rows = []
        for row in result["rows"]:
            row = [_ for i, _ in enumerate(row.split("|")) if i in to_keep_i]
            assert not rows or len(row) == len(rows[0]), 'Different sized rows'
            rows.append(row)
            # print(row)
        return header, rows, indexes

    @staticmethod
    def create_database(path):
        if os.path.isdir(path):
            raise DatabaseException("Directory already exists")
        os.makedirs(path)


class DatabaseTest(unittest.TestCase):
    def setUp(self):
        self.database_path = os.path.join(os.path.dirname(__file__), "../data/myDB/")
        self.database = Database(self.database_path)
        table = self.database.get_table('studenti')
        table.delete({})
        table.insert({"nr_matricol": "1001SL1023", "varsta": 23, "nume": "Ionescu", "prenume": "Gigel"})
        table.insert({"nr_matricol": "1001SL2034", "varsta": 22, "nume": "Popescu", "prenume": "Dorel"})
        table.insert({"nr_matricol": "1001SL2035", "varsta": 21, "nume": "Georgescu", "prenume": "Ionel"})
        table.insert({"nr_matricol": "1001SL2093", "varsta": 38, "nume": "Ackerman", "prenume": "Levi"})

    def test_select_standard(self):
        headers, rows, indexes = self.database.select(
            "select prenume, nume from studenti where varsta > 30")
        self.assertEqual(headers, ['nume', 'prenume'])
        self.assertEqual(rows, [['Ackerman', 'Levi']])
        self.assertEqual(indexes, [3])

    def test_raise_exception_on_missing_column(self):
        with self.assertRaises(DatabaseException):
            self.database.select("select prenume_fals from studenti")

    def test_database_get_tables(self):
        tables = self.database.get_tables()
        self.assertEqual('studenti' in tables, True)

    def test_database_get_table_select(self):
        table = self.database.get_table('studenti')
        self.assertEqual(os.path.basename(str(table)), 'studenti')

    def test_database_table_not_found(self):
        with self.assertRaises(DatabaseException):
            self.database.get_table('studenti falsi')


if __name__ == "__main__":
    unittest.main()
    exit()
    db = Database("../data/myDB/")
    # db.select("select * from studenti")
    # db.select("select prenume from studenti")
    print(db.select("select prenume, nume from studenti where varsta > 30"))
    # exit()
    table = db.get_table('studenti')
    table.select("select prenume from studenti")
