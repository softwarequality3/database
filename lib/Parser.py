import re
import unittest


class ParserException(Exception):
    def __init__(self, message):
        super().__init__(message)
        self.errors = ""
        self.message = message


query_re = re.compile(
    r"^\s*"
    r"SELECT\s+(?P<projection>.*?)"
    r"\s+"
    r"FROM\s+(?P<table>.*?)"
    r"(?:\s+WHERE\s+(?P<condition>.*?))?"
    r"\s*$",
    re.DOTALL | re.IGNORECASE)


def parse_select(query):
    assert hasattr(query, 'startswith'), "Query must be a string"
    m = query_re.match(query)
    if m is None:
        raise ParserException("Invalid select query: \"{}\"".format(query))
    assert len(m.groups()) == 3, "Invalid query"
    projection, table, condition = [g.strip() if g else "" for g in m.groups()]
    projection = list(map(str.strip, projection.split(",")))
    assert isinstance(projection, list)
    assert isinstance(table, str)
    assert isinstance(condition, str)
    return projection, table, condition


class TestParser(unittest.TestCase):
    def test_parse_select(self):
        columns, table, condition = parse_select(
            "SELECT COL1, COL2 FROM TABLE_NAME WHERE COL1=3 and COL2 LIKE /^dsa/")

        self.assertEqual(table, 'TABLE_NAME')
        self.assertEqual(len(columns), 2)
        self.assertEqual(columns, ['COL1', 'COL2'])
        self.assertEqual(condition, 'COL1=3 and COL2 LIKE /^dsa/')

    def test_parse_select_exception(self):
        with self.assertRaises(ParserException):
            parse_select('NOT A VALID SELECT QUERY')


if __name__ == "__main__":
    unittest.main()
    # print(parse_select("SELECT COL1, COL2 FROM TABLE_NAME WHERE COL1=3 and COL2 LIKE /^dsa/"))
    # print(parse_select("SELECT COL1, COL2 FROM TABLE_NAME"))
