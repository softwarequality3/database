import os


STRING_TO_TYPE = {
    "FLOAT": float,
    "STRING": str,
}


class TableException(Exception):
    def __init__(self, message):
        super().__init__(message)
        self.errors = ""
        self.message = message


class Table():
    """
    Clasa ce reprezinta o interfata pentru o baza de date
    """

    def _get_fields(self, fh):
        # Voi avea un bug daca am 2 field cu acelasi nume.
        assert hasattr(fh, 'read'), "Must give file handle as parameter"
        self._fields_arr = []
        self._fields = {}
        self._header = ""
        for line in fh:
            line = line.strip()
            field_name, field_type = line.split("|")
            field_name = field_name.lower()
            if field_type == "FLOAT":
                field_type = float
            elif field_type == "STRING":
                field_type = str
            else:
                raise "Invalid field_type"
            self._fields_arr.append({"name": field_name, "type": field_type})
            self._fields[field_name] = field_type
            self._header = self._header + " " + field_name
        assert len(self._header) > 0, "Header is null"
        assert len(self._fields) > 0, "No fields"
        assert len(self._fields_arr) > 0, "No fields"
        assert len(self._fields) == len(self._fields_arr), "Fields array length != Fields data length"

    def __init__(self, dir_path):
        """
        dir_path = path catre directorul unui db
        """
        assert os.path.isdir(dir_path), "Must give a valid directory path as param"
        self._dir_path = dir_path
        self._fields_arr = []
        self._fields = {}
        self._header = ""
        try:
            with open(os.path.join(dir_path, "metadata")) as f:
                self._get_fields(f)
        except:
            raise TableException("Can not read metadata file or bad format")

    def columns(self):
        return [f["name"] for f in self._fields_arr]

    def __str__(self):
        return self._dir_path

    def to_row(self, new_doc):
        assert len(new_doc) == len(self._fields), "Invalid number of fields"
        #Celelalte verificari le facem dupa, nu mai e nevoie de asert
        # Verificam daca nu are chei in plus sau de alt tip
        for key in new_doc:
            if not self._fields.get(key):
                raise "Invalid field %s in inserted document" % (key)
            if isinstance(new_doc.get(key), int):
                new_doc[key] = float(new_doc.get(key))
            if not isinstance(new_doc.get(key), self._fields.get(key)):
                raise "Invalid type of field %s in inserted document" % (key)
        # Verificam daca nu are chei lipsa
        for field_name in self._fields:
            if not new_doc.get(field_name):
                raise "Missing field %s" % (field_name)

        row = ''
        for field in self._fields_arr:
            if field.get("type") == float:
                value = new_doc.get(field.get("name"))
                row += str(value) + "|"
            elif field.get("type") == str:
                row += new_doc.get(field.get("name")) + "|"
        row = row[:-1]
        assert len(row) > 0, "Row empty"
        assert row.count('|') == len(self._fields) - 1, "Row has wrong number of separators"
        return row

    def from_row(self, row):
        # las aici un posibil bug cu cazul in care stringul are | in el.
        # print(row)
        assert hasattr(row, 'startswith'), "row must be string!"
        assert row.count("|") + 1 == len(self._fields_arr), "row must have correct number of |"
        try:
            splitted = row.split("|")
            ret_val = {}
            for item, field in zip(splitted, self._fields_arr):
                if field.get("type") == float:
                    ret_val[field.get("name")] = float(item)
                elif field.get("type") == str:
                    ret_val[field.get("name")] = item
                else:
                    raise TableException("Parsing row failed")
            assert len(ret_val) == row.count('|') + 1, "Return value has innvalid number of fields"
            return ret_val
        except:
            raise TableException("Parsing row failed")

    def insert(self, new_doc):
        # Prima data trebuie sa vedem ca new_doc are acelasi toate field-urile db-ului, si de tip corect.
        assert type(new_doc) == dict, "New doc must be dictionary"
        try:
            insert_row = self.to_row(new_doc)
        except:
            raise TableException("Inserted row not matching schema.")

        try:
            with open(os.path.join(self._dir_path, "data"), "a") as f:
                f.write(insert_row)
                f.write("\n")
        except:
            raise TableException("Can not open data file for writing")

    def doc_matches_query(self, doc, query):
        assert type(query) == dict, "query must be dict"
        for field in query:
            if field in doc:
                for query_type in query.get(field):
                    if query_type == "eq":
                        # Test egalitate.
                        if doc[field] != query.get(field).get("eq"):
                            return False
                    if query_type == "lte":
                        if doc[field] > query.get(field).get("lte"):
                            return False
                    if query_type == "lt":
                        if doc[field] >= query.get(field).get("lt"):
                            return False
                    if query_type == "gte":
                        if doc[field] < query.get(field).get("gte"):
                            return False
                    if query_type == "gt":
                        if doc[field] <= query.get(field).get("gt"):
                            return False
                    if query_type == "contains":
                        if not (query.get(field).get("contains") in doc[field]):
                            return False
        return True

    def query(self, query_doc):
        assert type(query_doc) == dict, "query must be dict"
        try:
            ret_val = {"rows": [], "indexes": []}
            with open(os.path.join(self._dir_path, "data"), "r") as f:
                idx = 0
                for row in f:
                    row = row.strip()
                    doc = self.from_row(row)
                    if self.doc_matches_query(doc, query_doc):
                        ret_val["rows"].append(row)
                        ret_val["indexes"].append(idx)
                    idx += 1
            return ret_val
        except:
            raise TableException("Failed to read data")

    def delete_at_indexes(self, indexes):
        # for idx in indexes:
        #     assert idx >= 0, "Indexes must be greater than 0"

        with open(os.path.join(self._dir_path, "data"), "r") as f:
            data_copy = f.readlines()
        data_copy_len = len(data_copy)
        assert all(0 <= idx < data_copy_len for idx in indexes), "Invalid range for indexes"
        assert len(indexes) == len(list(set(indexes))), "Indexes must not have duplicates"
        # for idx in indexes:
        #     assert idx < len(data_copy), "Index larger than data size"
        # indexes_set = set(indexes)
        # data_copy = [_ for i, _ in enumerate(data_copy) if i not in indexes_set]
        for index in sorted(indexes, reverse=True):
            assert index < len(data_copy), "Out of bounds popping"
            data_copy.pop(index)
        with open(os.path.join(self._dir_path, "data"), "w") as f:
            for row in data_copy:
                f.write(row)
        assert data_copy_len - len(indexes) == len(data_copy), "Invalid number of deleted items"

    def delete_at_index(self, index):
        self.delete_at_indexes([index])

    def delete(self, query_doc):
        where = self.query(query_doc)
        for row_idx in sorted(where.get("indexes"), reverse=True):
            self.delete_at_index(row_idx)

    def update(self, query_doc, update_doc):
        where = self.query(query_doc)
        # Stergem si refacem.
        self.delete_at_indexes(where.get("indexes"))
        # for row_idx in where.get("indexes"):
        #     self.delete_at_index(row_idx)
        for row in where.get("rows"):
            doc = self.from_row(row)
            for field in update_doc:
                operation = update_doc.get(field)
                if "set" in operation:
                    doc[field] = operation["set"]
                elif "add" in operation:
                    doc[field] += operation["add"]
                elif "dec" in operation:
                    doc[field] -= operation["dec"]
            self.insert(doc)

    def parse_query(self, query):
        assert hasattr(query, 'startswith'), "query must be string"
        query_doc = {}
        commands = query.split("and")
        for command in commands:
            command = command.strip()
            operator = None
            transformed_operator = None
            if "<=" in command:
                operator = "<="
                transformed_operator = "lte"
            elif ">=" in command:
                operator = ">="
                transformed_operator = "gte"
            elif "<" in command:
                operator = "<"
                transformed_operator = "lt"
            elif ">" in command:
                operator = ">"
                transformed_operator = "gt"
            elif "=" in command:
                operator = "="
                transformed_operator = "eq"
            elif "contains" in command:
                operator = "contains"
                transformed_operator = "contains"
            else:
                continue
            field_name, value = command.split(operator)
            field_name = field_name.strip()
            value = value.strip()
            if self._fields.get(field_name):
                if self._fields.get(field_name) == float:
                    value = float(value)
                query_doc[field_name] = {}
                query_doc[field_name][transformed_operator] = value
        return query_doc

    def parse_update(self, s):
        update_doc = {}
        assert hasattr(s, 'startswith'), "update string must be string"
        commands = s.split("and")
        for command in commands:
            command = command.strip()
            operator = None
            transformed_operator = None

            if "+=" in command:
                operator = "+="
                transformed_operator = "add"
            elif "-=" in command:
                operator = "-="
                transformed_operator = "dec"
            elif "=" in command:
                operator = "="
                transformed_operator = "set"
            else:
                continue
            field_name, value = command.split(operator)
            field_name = field_name.strip()
            value = value.strip()
            if self._fields.get(field_name):
                if self._fields.get(field_name) == float:
                    value = float(value)
                update_doc[field_name] = {}
                update_doc[field_name][transformed_operator] = value
        return update_doc

    @staticmethod
    def create_table(_dir_path, fields):
        assert not os.path.isdir(_dir_path), "Directory must not exist"
        if os.path.isdir(_dir_path):
            # Exista deja
            return
        os.makedirs(_dir_path)
        f = open(os.path.join(_dir_path, "data"), "w")
        f.close()
        with open(os.path.join(_dir_path, "metadata"), "w") as f:
            for field in fields:
                if field.get("type") == str:
                    field_type = "STRING"
                elif field.get("type") == float:
                    field_type = "FLOAT"
                else:
                    raise TableException("Invalid type")
                f.write("%s|%s\n" % (field.get("name"), field_type))
        assert os.path.isfile(os.path.join(_dir_path, "metadata")), "Metadata file not created"
        assert os.path.isfile(os.path.join(_dir_path, "data")), "Data file does not exist"

    def export(self, export_file, **kwargs):
        self.export_csv(export_file, **kwargs)

    def export_csv(self, export_file, **kwargs):
        self._export(export_file, ",", **kwargs)

    def export_text(self, export_file, **kwargs):
        self._export(export_file, "\t", **kwargs)

    def _export(self, export_file,
                sep=",",
                overwrite=False):
        with open(os.path.join(self._dir_path, "metadata")) as fd:
            names, types = zip(*[line.strip().split("|")
                                 for line in fd if "|" in line])
        with open(os.path.join(self._dir_path, "data")) as fd:
            data = fd.read().replace("|", sep)
        if not overwrite and os.path.exists(export_file):
            raise TableException(
                "Export destination {} already exists".format(
                    export_file))
        with open(export_file, "w") as fd:
            fd.write(sep.join(names))
            fd.write("\n")
            fd.write(sep.join(types))
            fd.write("\n")
            fd.write(data)
        assert os.path.isfile(export_file)

    @staticmethod
    def import_table(import_file, **kwargs):
        return Table.import_table_from_csv(import_file, **kwargs)

    @staticmethod
    def import_table_from_csv(import_file, **kwargs):
        return Table._import_table(import_file, ",", **kwargs)

    @staticmethod
    def _import_table(import_file, sep,
                      database_dir=".",
                      table_name=None,
                      overwrite=False):
        if table_name is None:
            table_name = os.path.splitext(os.path.basename(import_file))[0]
        table_path = os.path.join(database_dir, table_name)
        if not overwrite and os.path.exists(table_path):
            raise TableException(
                "Table with name {} already exists in database {}".format(
                    table_name, database_dir))
        os.makedirs(table_path)
        with open(import_file, "r") as fd:
            names = fd.readline().strip().split(sep)
            types = fd.readline().strip().split(sep)
            data = fd.read().replace(sep, "|")
        with open(os.path.join(table_path, "metadata"), "w") as fd:
            for n, t in zip(names, types):
                fd.write('{}|{}\n'.format(n, t))
        with open(os.path.join(table_path, "data"), "w") as fd:
            fd.write(data)
        return table_path
