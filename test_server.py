import os
import shutil
import lib.Database
import lib.Table


# Creere DB
try:
    shutil.rmtree("./data")
except:
    pass

import requests
URL = "http://127.0.0.1"
# Creere db
r = requests.post(URL, json={"command": "create database", "db_name": "myDB3"})
print(r.text)
# Selectare db
r = requests.post(URL, json={"command": "select database", "db_name": "myDB3"})
print(r.text)
# Creere tabel
data = {
    "command": "create table",
    "table_name": "studenti",
    "fields": [
        {"name": "nr_matricol", "type": "string"},
        {"name": "nume", "type": "string"},
        {"name": "prenume", "type": "string"},
        {"name": "varsta", "type": "float"}
    ]
}
r = requests.post(URL, json=data)
print(r.text)
# Selectare tabel
r = requests.post(URL, json={"command": "select table", "table_name": "studenti"})
print(r.text)

# insertii
print("=== Insert ===")
for_insert = [
    {"nr_matricol": "1001SL1023", "varsta": 23, "nume": "Ionescu", "prenume": "Gigel"},
    {"nr_matricol": "1001SL2034", "varsta": 22, "nume": "Popescu", "prenume": "Dorel"},
    {"nr_matricol": "1001SL2035", "varsta": 21, "nume": "Georgescu", "prenume": "Ionel"},
    {"nr_matricol": "1001SL2093", "varsta": 38, "nume": "Ackerman", "prenume": "Levi"}
]

for doc in for_insert:
    r = requests.post(URL, json={"command": "insert", "doc": doc})
    print(r.text)

print("====== Query ======")
print(" Varsta == 38 ")
r = requests.post(URL, json={"command": "query", "query_string": "varsta=38"})
print(r.text)

print(" Varsta <= 22 si prenume ce 'el' ")
r = requests.post(URL, json={"command": "query", "query_string": "varsta<=22 and prenume contains el"})
print(r.text)

print("====== Update ======")
print("Crestem varsta lui Levi")
r = requests.post(URL, json={"command": "update", "query_string": "prenume=Levi", "update_string": "varsta+=1"})
print(r.text)

print("Primtam tot db-ul")
r = requests.post(URL, json={"command": "query", "query_string": ""})
print(r.text)

print("====== Delete ======")
r = requests.post(URL, json={"command": "delete", "query_string": "varsta <= 21.3"})
print(r.text)
print("Primtam tot db-ul")
r = requests.post(URL, json={"command": "query", "query_string": ""})
print(r.text)
