import time
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import lib.Database
import lib.Table
import lib.Parser
import os
import shutil

HOST_NAME = "localhost"
PORT_NUMBER = 80
current_db = None
current_table = None
current_table_name = None
dirname = os.path.join(os.path.dirname(__file__), "data")


class Server(BaseHTTPRequestHandler):
    def do_HEAD(self):
        return

    def do_POST(self):
        global current_db
        global current_table
        global current_table_name

        content_len = int(self.headers.get('Content-Length'))
        post_body = self.rfile.read(content_len)
        data = json.loads(str(post_body.decode("utf-8")))
        command = data.get("command")
        #self.send_header('Content-Type', 'application/json')

        if command.lower() == "select database":
            db_name = data.get("db_name")
            if db_name and os.path.isdir(os.path.join(dirname, db_name)):
                current_db = db_name
                self.send_response(200)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "ok"}).encode(encoding="utf_8"))
                # self.wfile.close()
            else:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "Database not exists"}).encode(encoding="utf_8"))
                # self.wfile.close()
                print(current_db)
            return

        elif command.lower() == "select table":
            if not current_db:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "Select DB first"}).encode(encoding="utf_8"))
                # self.wfile.close()
                return
            table_name = data.get("table_name")
            if not os.path.exists(os.path.join(dirname, current_db, table_name)):
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "Table not exists"}).encode(encoding="utf_8"))
                # self.wfile.close()
                return
            current_table_name = table_name
            current_table = lib.Table.Table(os.path.join(dirname, current_db, current_table_name))
            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({"status": "ok"}).encode(encoding="utf_8"))
            # self.wfile.close()

        elif command.lower() == "delete database":
            if not current_db:
                self.send_response(400)
                self.wfile.write(json.dumps({"status": "Select DB first"}).encode(encoding="utf_8"))
                # self.wfile.close()
                return
            try:
                shutil.rmtree(os.path.join(dirname, current_db))
            except:
                pass
            current_db = None
            current_table = None
            current_table_name = None
            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({"status": "ok"}).encode(encoding="utf_8"))
            # self.wfile.close()

        elif command.lower() == "delete table":
            if not current_db:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "Select DB first"}).encode(encoding="utf_8"))
                # self.wfile.close()
                return
            try:
                shutil.rmtree(os.path.join(dirname, current_db, current_table_name))
            except:
                pass
            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({"status": "ok"}).encode(encoding="utf_8"))
            # self.wfile.close()

        elif command.lower() == "create database":
            db_name = data.get("db_name")
            if os.path.isdir(os.path.join(dirname, db_name)):
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "DB Already exists"}).encode(encoding="utf_8"))
                # self.wfile.close()
                return
            lib.Database.Database.create_database(os.path.join(dirname, db_name))
            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({"status": "ok"}).encode(encoding="utf_8"))
            # self.wfile.close()

        elif command.lower() == "get_fields":
            if not current_table:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "must select a table first"}).encode(encoding="utf_8"))
                # self.wfile.close()
            else:
                self.send_response(200)
                self.end_headers()
                fields = []
                for doc in current_table._fields_arr:
                    name = doc.get("name")
                    t = doc.get("type")
                    if t == str:
                        type_str = "string"
                    elif t == float:
                        type_str = "float"
                    fields.append({"name": name, "type": type_str})
                self.wfile.write(json.dumps({"status": "ok", "fields": fields}).encode(encoding="utf_8"))
                # self.wfile.close()
            return

        elif command.lower() == "create table":
            if not current_db:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "must select a database first"}).encode(encoding="utf_8"))
                # self.wfile.close()
                return
            fields = data.get("fields")
            # ar trebui sa fie array de {"name" : str, "type" : "string" sau "float"}
            for field in fields:
                if field.get("type") == "string":
                    field["type"] = str
                elif field.get("type") == "float":
                    field["type"] = float
            table_name = data.get("table_name")
            lib.Table.Table.create_table(os.path.join(dirname, current_db, table_name), fields)
            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({"status": "ok"}).encode(encoding="utf_8"))
            # self.wfile.close()
            return

        elif command.lower() == "insert":
            if not current_table:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "must select a table first"}).encode(encoding="utf_8"))
                # self.wfile.close()
                return
            insert_doc = data.get("doc")
            for field in current_table._fields:
                if current_table._fields[field] == float:
                    insert_doc[field] = float(insert_doc[field])
            current_table.insert(insert_doc)
            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({"status": "ok"}).encode(encoding="utf_8"))
            # self.wfile.close()
            return

        elif command.lower() == "query":
            if not current_table:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "must select a table first"}).encode(encoding="utf_8"))
                # self.wfile.close()
                return
            query_doc = current_table.parse_query(data.get("query_string"))
            results = current_table.query(query_doc)
            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({"status": "ok", "results": results}).encode(encoding="utf_8"))
            # self.wfile.close()
            return

        elif command.lower() == "delete":
            if not current_table:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "must select a table first"}).encode(encoding="utf_8"))
                # self.wfile.close()
                return
            query_doc = current_table.parse_query(data.get("query_string"))
            current_table.delete(query_doc)
            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({"status": "ok"}).encode(encoding="utf_8"))
            # self.wfile.close()
            return

        elif command.lower() == "update":
            if not current_table:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(json.dumps({"status": "must select a table first"}).encode(encoding="utf_8"))
                # self.wfile.close()
                return
            query_doc = current_table.parse_query(data.get("query_string"))
            update_doc = current_table.parse_update(data.get("update_string"))

            current_table.update(query_doc, update_doc)
            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({"status": "ok"}).encode(encoding="utf_8"))
            # self.wfile.close()
            return

    def do_GET(self):
        print(self.path)
        if self.path == '/':
            f = open("./client/index.html", "rb")
            self.wfile.write(f.read())
            return
        else:
            path = './client'
            path += self.path
            if os.path.isfile(path):
                f = open(path, "rb")
                self.wfile.write(f.read())
                return
            else:
                self.wfile.write(b"404")
                return

    def handle_http(self):
        return

    def respond(self):
        return


httpd = HTTPServer((HOST_NAME, PORT_NUMBER), Server)
print(time.asctime(), 'Server UP - %s:%s' % (HOST_NAME, PORT_NUMBER))
try:
    httpd.serve_forever()
except KeyboardInterrupt:
    pass
httpd.server_close()
print(time.asctime(), 'Server DOWN - %s:%s' % (HOST_NAME, PORT_NUMBER))
