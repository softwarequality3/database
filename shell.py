import lib.Database
import lib.Table
import lib.Parser
import os
import shutil


def print_available_commands():
    print("Available commands:")
    print("select database")
    print("select table")
    print("delete database")
    print("delete table")
    print("create database")
    print("create table")
    print("query")
    print("insert")
    print("update")
    print("delete")
    print("export")
    print("import")
    print("exit")


if __name__ == '__main__':
    print("Welcome to the Shell!")
    print("This shell uses the data files directly, not via API.")
    print_available_commands()
    dirname = os.path.join(os.path.dirname(__file__), "data")
    current_db = None
    current_table_name = None
    current_table = None

    while True:
        command = input(">>>")
        if command.lower() == "exit":
            break

        elif command.lower() == "help":
            print_available_commands()

        elif command.lower() == "database":
            print(current_db or "No database selected")
        elif command.lower() == "table":
            print(str(current_table) or "No table selected")

        elif command.lower() == "select database":
            db_name = input("Enter database name:")
            if not os.path.isdir(os.path.join(dirname, db_name)):
                print("Database does not exist")
            else:
                current_db = db_name

        elif command.lower() == "select table":
            if not current_db:
                print("First you must select a database")
                continue
            table_name = input("Enter table name:")
            if not os.path.exists(os.path.join(dirname, current_db, table_name)):
                print("Table does not exist in the current database")
                continue
            else:
                current_table_name = table_name
                current_table = lib.Table.Table(os.path.join(dirname, current_db, current_table_name))

        elif command.lower() == "delete database":
            if not current_db:
                print("First you must select a database")
                continue
            try:
                shutil.rmtree(os.path.join(dirname, current_db))
            except:
                pass
            current_db = None
            current_table = None
            current_table_name = None

        elif command.lower() == "delete table":
            if not current_db:
                print("First you must select a database")
                continue
            if not current_table_name:
                print("First you must select a table")
                continue

            try:
                shutil.rmtree(os.path.join(dirname, current_db, current_table_name))
                current_table = None
                current_table_name = None
            except:
                pass

        elif command.lower() == "create database":
            db_name = input("Enter database name:")
            if os.path.isdir(os.path.join(dirname, db_name)):
                print("Database already exists!")
                continue
            lib.Database.Database.create_database(os.path.join(dirname, db_name))

        elif command.lower() == "create table":
            if not current_db:
                print("First select a database")
                continue
            table_name = input("Enter table name:")
            fields = []
            print("Enter the fields of this table:")
            while True:
                print("Field number %d" % (len(fields) + 1))
                field_name = input("Enter field name (Just press enter if you're done):")
                if len(field_name) == 0:
                    break
                field_type = input("Enter field type (string or float)")
                if field_type not in ["string", "float"]:
                    print("Invalid field type. Try again")
                    continue
                if field_type == "string":
                    fields.append({"name": field_name, "type": str})
                elif field_type == "float":
                    fields.append({"name": field_name, "type": float})
            lib.Table.Table.create_table(os.path.join(dirname, current_db, table_name), fields)
        elif command.lower() == "insert":
            if not current_table:
                print("Select a table first")
                continue
            insert_doc = {}
            for field in current_table._fields_arr:
                value = input("Value for field %s:" % (field.get("name")))
                if field.get("type") == float:
                    try:
                        value = float(value)
                    except:
                        print("This is not a float. 0 will be inserted instead.")
                        value = 0.0
                insert_doc[field.get("name")] = value
            current_table.insert(insert_doc)
        elif command.lower() == "query":
            if not current_table:
                print("Select a table first")
                continue
            # Sintaxa: field operator valoare and field operator valoare ...
            query = input("query:")
            query_doc = current_table.parse_query(query)
            results = current_table.query(query_doc)
            print(current_table._header)
            for row in results.get("rows"):
                print(row)
        elif command.lower() == "delete":
            if not current_table:
                print("Select a table first")
                continue
            query = input("query:")
            query_doc = current_table.parse_query(query)
            current_table.delete(query_doc)
        elif command.lower() == "update":
            if not current_table:
                print("Select a table first")
                continue
            # La update sunt 2 inputuri: Unul ca la query si unul ceva de genul field=value and field+=value and field-=value
            query = input("query:")
            query_doc = current_table.parse_query(query)
            update = input("update:")
            update_doc = current_table.parse_update(update)
            current_table.update(query_doc, update_doc)
        elif command.lower() == "export":
            if not current_table:
                print("Select a table first")
                continue
            export_file = input("where to export:")
            current_table.export(export_file, overwrite=True)
        elif command.lower() == "import":
            import_file = input("data to be imported:")
            table_name = lib.Table.Table.import_table(
                import_file, database_dir=current_db)
            print("Imported table {} to datbase {}".format(
                table_name, current_db, overwrite=True))

        elif command.lower().startswith("select"):
            if not current_db:
                print("Select a database first")
                continue
            try:
                db_handle = lib.Database.Database(os.path.join(dirname, current_db))
                header, rows, idxs = db_handle.select(command)
                print('\t'.join(['Id'] + header))
                for i, row in zip(idxs, rows):
                    print('\t'.join([str(i)] + row))
            except Exception as e:
                print(e)
                raise

    print("Bye!")
