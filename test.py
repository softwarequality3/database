import os
import shutil
import lib.Database
import lib.Table


# Creere DB
try:
    shutil.rmtree("./data")
except:
    pass

lib.Database.Database.create_database("./data/myDB")

fields = [
    {"name": "nr_matricol", "type": str},
    {"name": "varsta", "type": float},
    {"name": "nume", "type": str},
    {"name": "prenume", "type": str}
]
lib.Table.Table.create_table("./data/myDB/studenti", fields)

db = lib.Table.Table("./data/myDB/studenti")

# Insertii
db.insert({"nr_matricol": "1001SL1023", "varsta": 23, "nume": "Ionescu", "prenume": "Gigel"})
db.insert({"nr_matricol": "1001SL2034", "varsta": 22, "nume": "Popescu", "prenume": "Dorel"})
db.insert({"nr_matricol": "1001SL2035", "varsta": 21, "nume": "Georgescu", "prenume": "Ionel"})
db.insert({"nr_matricol": "1001SL2093", "varsta": 38, "nume": "Ackerman", "prenume": "Levi"})

# Query
# varsta == 38
print("====== Query ======")
print(" Varsta == 38 ")
print(db.query({"varsta": {"eq": 38}}))
# varsta <= 22
print("Varsta <= 22 ")
print(db.query({"varsta": {"lte": 22}}))
# prenumenume cu "el" in el
print("Prenume ce contine 'el'")
print(db.query({"prenume": {"contains": "el"}}))

print("====== Update ======")
# update: Levi age ++
print(" Crestem varsta lui Levi cu 1 apoi query. Daca merge asta merge si insert si delete ")
db.update({"prenume": {"eq": "Levi"}}, {"varsta": {"add": 1}})
print(db.query({"prenume": {"eq": "Levi"}}))

# export/import
db.export_csv('temp.csv', overwrite=True)
lib.Table.Table.import_table('temp.csv')
